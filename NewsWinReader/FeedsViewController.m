//
//  FeedsViewController.m
//  NewsWinReader
//
//  Created by Volodymyr Viniarskyi on 1/26/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "FeedsViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "FacebookService.h"
@interface FeedsViewController ()
@property (strong, nonatomic) FacebookService *fBService;
@end

@implementation FeedsViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.fBService = [[FacebookService alloc] init];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self testRACimplemenataion];
    // Do any additional setup after loading the view.
}
- (void) testRACimplemenataion {
    [[[self.fBService.getFeedsSignal deliverOn:[RACScheduler scheduler]]
      doNext:^(id x) {
          NSLog(@"x:%@", x);
      }]
     subscribeNext:^(id x) {
         NSLog(@"next x:%@", x);
     }];
    self.nextButton.rac_command = [[RACCommand alloc] initWithSignalBlock:^RACSignal *(id input) {
        [self.fBService.getFeedsSignal subscribeNext:^(id x) {
            NSLog(@"Button touch request:%@",x);
        }];
        return [RACSignal empty];
    }];
}
@end
