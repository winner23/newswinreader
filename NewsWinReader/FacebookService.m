//
//  FasebookService.m
//  NewsWinReader
//
//  Created by Volodymyr Viniarskyi on 1/25/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "FacebookService.h"
#import "ReactiveCocoa/ReactiveCocoa.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKCoreKit/FBSDKGraphRequest.h>
@interface FacebookService()
@property (strong, nonatomic) FBSDKLoginManager *loginManager;
@end
@implementation FacebookService
+ (id)shared {
    static FacebookService *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.loginManager = [[FBSDKLoginManager alloc]init];
    }
    return self;
}
- (RACSignal *) requestTokenForViewController:(UIViewController *) viewController {
    RACSignal *newSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        [self.loginManager logInWithReadPermissions:@[@"public_profile"] fromViewController:viewController handler:
         ^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error) {
                NSLog(@"Error: %@", error.description);
                [subscriber sendError:error];
                self.loggedin = NO;
            } else if (!result.isCancelled) {
                self.loggedin = YES;
                [subscriber sendNext:result];
                [subscriber sendCompleted];
            } else {
                self.loggedin = NO;
            }
         }];
        return nil;
    }];
    return newSignal;
}
- (RACSignal *) getFeedsSignal {
    RACSignal *signalFeeds = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSDictionary *parameters = @{@"fields": @"id,admin_creator,application,call_to_action,caption,created_time,description,feed_targeting,from,icon,is_hidden,is_published,link,message,message_tags,name,object_id,picture,place,privacy,properties,shares,source,status_type,story,story_tags,targeting,to,type,updated_time,with_tags"};
        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me?fields" parameters:parameters HTTPMethod:@"GET"];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if(error) {
                [subscriber sendError:error];
            } else {
                [subscriber sendNext:result];
                NSLog(@"result:%@",result);
            }
            [connection start];
        }];
        return [RACDisposable disposableWithBlock:^{
            NSLog(@"Canceled");
        }];
    }];
    return signalFeeds;
}

@end
