//
//  ViewController.m
//  NewsWinReader
//
//  Created by Volodymyr Viniarskyi on 1/25/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import "ViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "FacebookService.h"
@interface ViewController ()
@property (strong, nonatomic) FacebookService *fBService;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fBService = [[FacebookService alloc]init];
    RAC(self, loginButton.titleLabel.text) = [RACObserve(self, fBService) map:^id(FacebookService *fb) {
        return fb.loggedin ? @"Login" : @"Logout";
    }];
    [self.loginButton addTarget:self action:@selector(loginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
}
-(void)loginButtonClicked {
    [[self.fBService requestTokenForViewController:self] subscribeNext:^(id x) {
        NSLog(@"Logedin");
        [self performSegueWithIdentifier:@"login" sender:self];
    }];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"login"]) {
        [self.loginButton setEnabled:NO];
    }
}
@end
