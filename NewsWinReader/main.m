//
//  main.m
//  NewsWinReader
//
//  Created by Volodymyr Viniarskyi on 1/25/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
