//
//  FasebookService.h
//  NewsWinReader
//
//  Created by Volodymyr Viniarskyi on 1/25/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RACSignal;

@interface FacebookService : NSObject
@property (assign, nonatomic) BOOL loggedin;
+ (id)shared;
- (RACSignal *) requestTokenForViewController:(UIViewController *) viewController;
- (RACSignal *) getFeedsSignal;
@end
