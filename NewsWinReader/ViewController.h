//
//  ViewController.h
//  NewsWinReader
//
//  Created by Volodymyr Viniarskyi on 1/25/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBSDKLoginKit/FBSDKLoginKit.h"
@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@end

