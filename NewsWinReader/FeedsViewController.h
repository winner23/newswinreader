//
//  FeedsViewController.h
//  NewsWinReader
//
//  Created by Volodymyr Viniarskyi on 1/26/18.
//  Copyright © 2018 Volodymyr Viniarskyi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *feedsTable;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end
